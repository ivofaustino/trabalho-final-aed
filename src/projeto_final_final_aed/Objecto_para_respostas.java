package projeto_final_final_aed;
public class Objecto_para_respostas {

	int valor_maior,valor_menor;//O valor_maior é o valor do salário mais alto que a empresa paga actualmente
	int valor_maior_do_departamento,valor_menor_do_departamento;//O valor_maior_do_departamento é o valor mais alto do total salário por departamento
	int cargo_do_valor_maior,cargo_do_valor_Menor;//O numero_funcionarios_valor_Maior Quantas pessoas têm o salário mais alto
	int cargo_do_valor_maior_do_departamento,cargo_do_valor_Menor_do_departamento;//O numero_funcionarios_valor_Maior Quantas pessoas têm o salário mais alto
	
	//construtor
	public Objecto_para_respostas(){
		
	}
	

	//getters e setters
	
	
	
	public int getValor_maior_do_departamento() {
		return valor_maior_do_departamento;
	}

	public int getCargo_do_valor_maior_do_departamento() {
		return cargo_do_valor_maior_do_departamento;
	}


	public void setCargo_do_valor_maior_do_departamento(int cargo_do_valor_maior_do_departamento) {
		this.cargo_do_valor_maior_do_departamento = cargo_do_valor_maior_do_departamento;
	}


	public int getCargo_do_valor_Menor_do_departamento() {
		return cargo_do_valor_Menor_do_departamento;
	}


	public void setCargo_do_valor_Menor_do_departamento(int cargo_do_valor_Menor_do_departamento) {
		this.cargo_do_valor_Menor_do_departamento = cargo_do_valor_Menor_do_departamento;
	}


	public void setValor_maior_do_departamento(int valor_maior_do_departamento) {
		this.valor_maior_do_departamento = valor_maior_do_departamento;
	}

	public int getValor_menor_do_departamento() {
		return valor_menor_do_departamento;
	}

	public void setValor_menor_do_departamento(int valor_menor_do_departamento) {
		this.valor_menor_do_departamento = valor_menor_do_departamento;
	}

	public int getValor_maior() {
		return valor_maior;
	}

	public void setValor_maior(int valor_maior) {
		this.valor_maior = valor_maior;
	}

	public int getValor_menor() {
		return valor_menor;
	}

	public void setValor_menor(int valor_menor) {
		this.valor_menor = valor_menor;
	}

	public int getCargo_do_valor_maior() {
		return cargo_do_valor_maior;
	}

	public void setCargo_do_valor_maior(int cargo_do_valor_maior) {
		this.cargo_do_valor_maior = cargo_do_valor_maior;
	}

	public int getCargo_do_valor_Menor() {
		return cargo_do_valor_Menor;
	}

	public void setCargo_do_valor_Menor(int cargo_do_valor_Menor) {
		this.cargo_do_valor_Menor = cargo_do_valor_Menor;
	}

}
