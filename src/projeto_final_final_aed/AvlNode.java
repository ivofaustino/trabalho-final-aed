package projeto_final_final_aed;
public class AvlNode {
	protected int height;       // Height
	protected long key;
	protected AvlNode left, right;
	protected Funcionario funcionario;

	public AvlNode ( long theElement ,Funcionario funcionario) {
		this( theElement, null, null, funcionario );
	}
	
	public AvlNode ( long cc_valor, AvlNode lt, AvlNode rt, Funcionario funcionario) {
		key = cc_valor;
		left = lt;
		right = rt;
		height   = 0;
		this.funcionario=funcionario;
	}
	
}

    